//! Render Markdown or reStructuredText with syntax highlighting and image filtering similar to GitHub's
//!
//! ```rust
//! use render_readme::*;
//! let r = Renderer::new(Highlighter::new());
//! let _ = r.markdown_str("# Hello", false);
//! let _ = r.page(&Markup::Markdown("# Hello".to_string()), None, true);
//! ```
//!
//! Markdown is implemented natively in Rust. RST rendering requires `rst2html` installed.
//!
extern crate ammonia;
extern crate regex;
extern crate simple_cache;
#[macro_use] extern crate lazy_static;
extern crate syntect;
extern crate tempdir;
extern crate comrak;
#[macro_use] extern crate html5ever;
extern crate markup5ever;
extern crate serde;
#[macro_use] extern crate serde_derive;

mod markup;
mod highlight;
mod parser;
mod filter;

pub use markup::*;
pub use filter::*;

pub use highlight::Highlighter;

/// Describe format/syntax used in the string
#[derive(Debug, Clone)]
pub enum Markup {
    Markdown(String),
    Rst(String),
}

/// Readme page from a repository
#[derive(Debug, Clone)]
pub struct Readme {
    pub markup: Markup,
    pub base_url: Option<String>,
    pub base_image_url: Option<String>,
}

impl Readme {
    pub fn new(markup: Markup, base_url: Option<String>, base_image_url: Option<String>) -> Self {
        Self {
            markup,
            base_url,
            base_image_url,
        }
    }
}
