use std::sync::Arc;
use std::error::Error;
use std::thread;
use Markup;
use filter::ImageFilter;
use comrak;
use parser::DomHighlighter;
use std::process::{Command, Stdio};
use std::io::Write;
use std::iter;
use std::fs::write;
use tempdir;
use ammonia::{Builder, UrlRelative, Url};
use Highlighter;

pub type ArcImageFilter = Arc<ImageFilter>;

const RST_TEMPLATE: &[u8] = include_bytes!("../rst_template.txt");

/// Convert and filter markup
pub struct Renderer {
    hilite: Highlighter,
    image_filter: Arc<ImageFilter>,
}

impl Renderer {
    pub fn new(hilite: Highlighter) -> Self {
        Self::new_filter(hilite, Arc::new(()))
    }

    /// See `ImageFilter`
    pub fn new_filter(hilite: Highlighter, image_filter: Arc<ImageFilter>) -> Self {
        Self {
            hilite,
            image_filter,
        }
    }

    /// Block content
    ///
    /// Base url is for links and images, respectively.
    pub fn page(&self, markup: &Markup, base_url: Option<(&str, &str)>, nofollow: bool) -> String {
        let html = match markup {
            Markup::Markdown(ref s) => {
                self.render_markdown(s, false)
            },
            Markup::Rst(ref s) => {
                self.render_rst(s.to_owned()).expect("rst2html failed. Is docutils installed?")
            },
        };
        let mut am = Builder::default();
        am.add_generic_attributes(iter::once("align"));
        self.clean_html(&html, am, base_url, nofollow)
    }

    fn render_rst(&self, markup: String) -> Result<String, Box<Error + Send + Sync + 'static>> {
        lazy_static!{
            static ref TEMP_TPL_ARG: String = {
                let tmp = tempdir::TempDir::new("crates-rs-rst").unwrap();
                let path = tmp.into_path().join("rst_template.txt");
                write(&path, RST_TEMPLATE).unwrap();
                format!("--template={}", path.display())
            };
        };

        let mut cmd = Command::new("rst2html")
                .arg("--strip-comments")
                .arg("--input-encoding=UTF-8:strict")
                .arg("--no-file-insertion")
                .arg("--quiet")
                .arg("--no-toc-backlinks")
                .arg("--no-doc-info")
                .arg("--no-doc-title")
                .arg(TEMP_TPL_ARG.as_str())
                .arg("--initial-header-level=2")
                .stdin(Stdio::piped())
                .stdout(Stdio::piped())
                .spawn()?;
        let mut stdin = cmd.stdin.take().ok_or("cmd stdin")?;
        let writer = thread::spawn(move || {
            stdin.write_all(markup.as_bytes())
        });

        let output = cmd.wait_with_output()?;

        if !output.status.success() {
            Err(format!("rst2html reported error: {}", String::from_utf8_lossy(&output.stderr)))?;
        }
        writer.join().unwrap()?;
        Ok(String::from_utf8(output.stdout)?)
    }

    /// Inline string
    pub fn markdown_str(&self, markdown: impl AsRef<str>, allow_links: bool) -> String {
        let mut b = Builder::default();
        b.rm_tags([
            "a",
            "p","div","h1","h2","h3","h4","h5","blockquote","col",
            "dd","dt","figure","table","td","tr","footer","header","hr",
            "li","ul","ol","nav","pre",
        ].iter().cloned().skip(if allow_links {1} else {0}));
        self.clean_html(&self.render_markdown(markdown.as_ref(), true), b, None, true)
    }

    fn render_markdown(&self, markdown: &str, inline: bool) -> String {
        comrak::markdown_to_html(markdown, &comrak::ComrakOptions {
            smart: inline,
            safe: inline, // we'll filter anyway
            github_pre_lang: true,
            ext_superscript: false,
            ext_autolink: true,
            ext_strikethrough: true,
            ext_table: true,
            ext_tagfilter: true,
            ext_tasklist: true,
            ext_header_ids: Some("readme-".to_string()),
            ext_footnotes: false,
            ..comrak::ComrakOptions::default()
        })
    }

    fn filter_html(&self, html: &str) -> String {
        let dh = DomHighlighter::new(&self.hilite, html, &*self.image_filter);
        dh.filtered()
    }

    fn clean_html(&self, unsafe_html: &str, mut am: Builder, base_url: Option<(&str, &str)>, nofollow: bool) -> String {
        if let Some((base_url, base_image_url_str)) = base_url {
            if let Ok(base_image_url) = Url::parse(base_image_url_str) {
                let base_url = base_url.to_owned();
                let base_image_url_str = base_image_url_str.to_owned();
                let image_filter = self.image_filter.clone();
                am.attribute_filter(move |element, attribute, value| {
                    match (element, attribute) {
                        ("a", "href") => {
                            let prefix = "https://crates.io/crates";
                            if value.starts_with(prefix) {
                                let unprefixed = &value[prefix.len()..];
                                return Some(format!("https://crates.rs/crates{}", unprefixed).into())
                            }
                        },
                        ("img", "src") => {
                            let tmp;
                            let rewritten = if value.starts_with(&base_url) && base_url != base_image_url_str {
                                tmp = base_image_url_str.to_owned() + &value[base_url.len()..];
                                &tmp
                            } else {value};
                            if let Ok(s) = base_image_url.join(rewritten) {
                                return Some(image_filter.filter_url(s.as_str()).to_string().into())
                            }
                        },
                        _ => {},
                    }
                    Some(value.into())
                });
            }
        }

        am.url_relative(Self::url_policy(base_url));
        am.link_rel(Some(if nofollow {"noopener nofollow"} else {"noopener"}));
        am.id_prefix(Some("user-"));
        am.rm_tags(["center", "map", "hgroup"].iter().cloned());
        let s = am.clean(unsafe_html).to_string();
        self.filter_html(&s)
    }

    fn url_policy(base_url: Option<(&str, &str)>) -> UrlRelative {
        if let Some((url, _)) = base_url {
            if let Ok(url) = Url::parse(url) {
                if !url.cannot_be_a_base() {
                    return UrlRelative::RewriteWithBase(url);
                }
            }
        }
        UrlRelative::Deny
    }
}

#[test]
fn rewrite_urls() {
    let r = Renderer::new(Highlighter::new());
    let v = r.page(&Markup::Markdown("[helo](https://crates.io/crates/hi)".to_owned()), Some(("http://test", "http://test")), false);
    assert_eq!(r#"<p><a href="https://crates.rs/crates/hi""#, &v[0..40]);
}

#[test]
fn rewrite_img_urls() {
    use std::borrow::Cow;
    struct Foo;
    impl ImageFilter for Foo {
        fn filter_url<'a>(&self, url: &'a str) -> Cow<'a, str> {
            format!("https://proxy/{}", url).into()
        }
        fn image_size(&self, _url: &str) -> Option<(u32, u32)> {
            Some((800, 600))
        }
    }

    let r = Renderer::new_filter(Highlighter::new(), Arc::new(Foo));
    let v = r.page(&Markup::Markdown("![helo](https://example.com/html/img.jpg)".to_owned()), Some(("https://example.com/html", "https://example.com/images")), false);
    assert_eq!(r#"<p><img src="https://proxy/https://example.com/images/img.jpg" alt="helo" width="800" height="600"></p>"#, v.trim());
}

#[test]
fn render_rst_test() {
    let r = Renderer::new(Highlighter::new());
    let v = r.page(&Markup::Rst(r#"
=====
hello
=====
world
"#.to_owned()), None, false);
    assert_eq!(r#"<div><div><h2>hello</h2><p>world</p></div></div>"#, &v.replace('\n',""));
}
