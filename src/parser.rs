use filter::ImageFilter;
use html5ever::driver::*;
use html5ever::QualName;
use html5ever::rcdom::{Handle, NodeData, RcDom};
use html5ever::tree_builder::TreeSink;
use html5ever::interface::Attribute;
use markup5ever::tendril::TendrilSink;
use html5ever::serialize::{Serialize, SerializeOpts, TraversalScope};
use html5ever::serialize::serialize as serialize5;
use html5ever::tree_builder::{TreeBuilderOpts, QuirksMode};
use html5ever::tokenizer::TokenizerOpts;
use std::io;
use Highlighter;

/// Internal ugly hack
pub struct DomHighlighter<'a> {
    dom: RcDom,
    hilite: &'a Highlighter,
    image_filter: &'a ImageFilter,
}

impl<'a> DomHighlighter<'a> {
    pub fn new(hilite: &'a Highlighter, markup: &str, image_filter: &'a ImageFilter) -> Self {
        let dom = parse(markup);
        let mut hi = Self {
            dom,
            hilite,
            image_filter,
        };
        let mut doc = hi.dom.document.clone();
        hi.filter_node(&mut doc, None);
        hi
    }

    pub fn filtered(&self) -> String {
        serialize_doc(&self.dom).expect("serialize")
    }

    fn filter_node(&mut self, node: &mut Handle, lang: Option<&str>) {
        let mut new_lang = None;
        match node.data {
            NodeData::Element { ref name, ref attrs, ..} if &*name.local == "pre" => {
                if let Some(attr) = get_attr("lang", &attrs.borrow()) {
                    new_lang = Some(attr.value.to_string());
                }
            },
            NodeData::Element { ref name, ..} if &*name.local == "code" => {
                // code class=\"language-rust\">
                let mut out = String::new();
                for child in node.children.borrow().iter() {
                    match child.data {
                        NodeData::Text{ref contents} => {
                            out += &self.hilite.highlight_as_html(&*contents.borrow(), lang);
                        },
                        _ => {},
                    }
                }
                let reparsed = parse(&out);
                let tmp = node.children.borrow().iter().rev().cloned().collect::<Vec<_>>();
                for t in &tmp {
                    self.dom.remove_from_parent(t);
                }
                self.dom.reparent_children(&reparsed.document, node);
                return; // important! otherwise may loop forever
            },
            NodeData::Element { ref name, ref attrs, ..} if &*name.local == "img" => {
                let mut attrs = attrs.borrow_mut();
                if get_attr("width", &attrs).is_none() {
                    let meta = if let Some(src) = get_attr("src", &attrs) {
                        self.image_filter.image_size(&src.value)
                    } else {
                        None
                    };
                    if let Some((width, height)) = meta {
                        attrs.push(Attribute {
                            name: QualName::new(None, ns!(), local_name!("width")),
                            value: format!("{}", width).into(),
                        });
                        attrs.push(Attribute {
                            name: QualName::new(None, ns!(), local_name!("height")),
                            value: format!("{}", height).into(),
                        });
                    }
                }
            },
            _ => {}
        }

        let lang = new_lang.as_ref().map(|s| s.as_str()).or(lang);
        for mut ch in node.children.borrow_mut().iter_mut() {
            self.filter_node(&mut ch, lang);
        }
    }
}

pub fn get_attr<'a>(name: &str, att: &'a [Attribute]) -> Option<&'a Attribute> {
    att.iter().find(|a| &*a.name.local == name)
}

pub fn parse(src: &str) -> RcDom {
    let parser = parse_fragment(
        RcDom::default(),
        ParseOpts {
            tokenizer: TokenizerOpts::default(),
            tree_builder: TreeBuilderOpts {
                exact_errors: false,
                scripting_enabled: false,
                iframe_srcdoc: false,
                drop_doctype: false,
                ignore_missing_rules: true,
                quirks_mode: QuirksMode::NoQuirks,
            },
        },
        QualName::new(None, ns!(html), local_name!("div")),
        vec![],
    );
    parser.one(src)
}

pub fn serialize_doc(doc: &RcDom) -> io::Result<String> {
    let children = doc.document.children.borrow();
    assert_eq!(1, children.len());
    serialize_node(&children[0])
}

pub fn serialize_node<S: Serialize>(node: &S) -> io::Result<String> {
    let mut out = Vec::new();
    serialize5(&mut out, node, SerializeOpts {
        scripting_enabled: false,
        traversal_scope: TraversalScope::ChildrenOnly(Some(QualName::new(None, ns!(html), local_name!("div")))),
        create_missing_parent: true,
    })?;

    Ok(String::from_utf8(out).map_err(|_| io::ErrorKind::InvalidData)?)
}

#[test]
fn roundtrip() {
    assert_eq!(r#"<h1 lang="pl">hell<noscript>o</noscript><!-- --></h1><p>hi</p>"#,
        &serialize_doc(&parse("<h1 lang=pl>hell<noscript>o</noscript><!-- --></h1><p>hi")).unwrap());
}

#[test]
fn code() {
    let h = Highlighter::new();
    let d = DomHighlighter::new(&h, "<pre><code>fn main(){}</code></pre>", &());
    assert_eq!("<pre><code><html><span class=\"src-rs\"><span class=\"m-fn m-fn-rs\"><span class=\"stor-ty stor-ty-fn\">fn</span> <span class=\"ent-n ent-n-fn\">main</span></span><span class=\"m-fn m-fn-rs\"><span class=\"m-fn m-fn-parms\"><span class=\"pun-sec pun-sec-parms\">(</span></span><span class=\"m-fn m-fn-parms\"><span class=\"pun-sec pun-sec-parms\">)</span></span></span><span class=\"m-fn m-fn-rs\"></span><span class=\"m-fn m-fn-rs\"><span class=\"m-bl m-bl-rs\"><span class=\"pun-sec pun-sec-bl\">{</span></span><span class=\"m-bl m-bl-rs\"><span class=\"pun-sec pun-sec-bl\">}</span></span></span></span></html></code></pre>",
        d.filtered());
}
