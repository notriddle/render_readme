
use regex::Regex;
use syntect::parsing::*;
use std::sync::Mutex;

/// `SyntaxSet` *is* unsafe to use across threads
/// so here we promise to use the mutex and not leak
/// the borrowed reference out of the mutex
unsafe impl Sync for Highlighter {}

/// Wrapper for a collection of Syntect syntaxes
pub struct Highlighter {
    syntaxes: Mutex<SyntaxSet>,
}

// It'd be nice to have more syntaxes
const TOML_SYNTAX: &str = include_str!("../TOML.sublime-syntax");
const RSMACRO_SYNTAX: &str = include_str!("../RustMacroName.sublime-syntax");

impl Highlighter {
    pub fn new() -> Self {
        let mut ss = SyntaxSet::load_defaults_nonewlines();
        ss.add_syntax(SyntaxDefinition::load_from_str(TOML_SYNTAX, false, None).unwrap());
        ss.add_syntax(SyntaxDefinition::load_from_str(RSMACRO_SYNTAX, false, None).unwrap());
        ss.link_syntaxes();

        Self {
            syntaxes: Mutex::new(ss),
        }
    }

    pub fn highlight_as_html(&self, code: &str, language: Option<&str>) -> String {

        let syntaxes = &*self.syntaxes.lock().unwrap();
        let lang = language.and_then(|l| {
            syntaxes.find_syntax_by_name(name_for_language(l))
            .or_else(|| {
                syntaxes.find_syntax_by_token(l) // same as extension but case insensitive
            }).or_else(|| {
                eprintln!("¶ unknown language: {}", l);
                None
            })
        }).or_else(|| {
            find_syntax_by_first_line(syntaxes, code.trim_left())
        }).unwrap_or_else(|| {
            syntaxes.find_syntax_plain_text()
        });

        html_for_string(code, lang)
    }
}

fn find_syntax_by_first_line<'a>(syntaxes: &'a SyntaxSet, code: &str) -> Option<&'a SyntaxDefinition> {
    lazy_static! {
        static ref PATH_LIKE: Regex = Regex::new(r#"^(?:\.\.?|~)?/\.?[a-zA-Z]\S+$|^[CD]:[/\\]"#).unwrap();
        static ref TOML_LIKE: Regex = Regex::new(r#"^\[[a-z][a-z._-]+\]"#).unwrap();
        static ref SH_LIKE: Regex = Regex::new(r#"^(?:export [A-Z]|set [A-Z]|(?:cargo|make|git|cc|gcc|vcpkg|pacman|curl|tar|cd|brew|xcode-select|rustup|rustc|sudo|apt-get|yum) [a-z+-]|--[a-z]|\$[A-Z]|\$ |\.\.?/[a-z])"#).unwrap();
        static ref HYPHENATED_WORD: Regex = Regex::new(r#"^[a-z][a-z0-9_]*-[a-zA-Z0-9][a-zA-Z0-9_-]*$"#).unwrap();
        static ref CMD_HELP: Regex = Regex::new(r#"^USAGE:|^test result:|\.\.\. bench:"#).unwrap();
        static ref MACRO: Regex = Regex::new(r#"^[a-z_]+!$"#).unwrap();
    }

    if MACRO.is_match(code) {
        return Some(syntaxes.find_syntax_by_name("RustMacroName").unwrap());
    }

    if code.starts_with("MIT/Apache") || PATH_LIKE.is_match(code) || HYPHENATED_WORD.is_match(code) || CMD_HELP.is_match(code) {
        return None;
    }
    if TOML_LIKE.is_match(code) {
        return syntaxes.find_syntax_by_name("TOML");
    }
    if SH_LIKE.is_match(code) {
        return syntaxes.find_syntax_by_extension("sh");
    }
    return syntaxes.find_syntax_by_first_line(code)
    .or_else(|| {
        syntaxes.find_syntax_by_name("Rust")
    });
}

/// Maps whatever name happens to be in code block tag to one of supported syntaxes
fn name_for_language(l: &str) -> &str {
    match l.split(',').next().unwrap().to_lowercase().as_str() {
        "rust" | "notest" | "no_run" | "should_panic" => "Rust",
        "c" | "C99" => "C",
        "toml" | "ini" | "cargo" => "TOML",
        "html" => "HTML",
        "diff" => "Diff",
        "json" => "JSON",
        "js" | "json5" => "JavaScript",
        "markdown" | "md" => "Markdown",
        "ignore" | "text" | "plain" | "plaintext" | "none" => "Plain Text",
        "sh" | "bash" | "shell" => "Bourne Again Shell (bash)",
        "Batchfile" => "Bourne Again Shell (bash)",
        "Dockerfile" => "Bourne Again Shell (bash)",
        "console" | "terminal" | "shell-session" => "Bourne Again Shell (bash)",
        "yaml" => "YAML",
        _ => l,
    }
}



fn html_for_string(s: &str, syntax: &SyntaxDefinition) -> String {
    let mut output = String::new();
    let mut parser = ParseState::new(syntax);

    let mut open_scopes: Vec<Option<String>> = Vec::with_capacity(12);
    let mut iter = s.split('\n').peekable();
    while let Some(line) = iter.next() {
        let mut prev_off = 0;
        let parsed = parser.parse_line(line);
        for (off, state) in parsed {
            if off > prev_off {
                for ch in line[prev_off..off].chars() {
                    match ch {
                        '&' => output.push_str("&amp;"),
                        '<' => output.push_str("&lt;"),
                        '"' => output.push_str("&quot;"),
                        ch => output.push(ch),
                    }
                }
                prev_off = off;
            }
            match state {
                ScopeStackOp::Push(scope) => {
                    let name = scope.build_string();
                    let mut short_name2 = String::with_capacity(name.len());
                    let mut short_name3 = String::with_capacity(name.len());
                    let mut p = name.split('.').enumerate().take(3).peekable();
                    while let Some((n, part)) = p.next() {
                        let part = abbreviate_scope_name(part);
                        short_name3.push_str(part);
                        if n < 2 {
                            short_name2.push_str(part);
                        }
                        if p.peek().is_some() {
                            short_name3.push('-');
                            if n < 1 {
                                short_name2.push('-');
                            }
                        }
                    }

                    let dupe = open_scopes.iter().filter_map(|s| s.as_ref()).any(|s| *s == short_name3);
                    let useless = dupe || match short_name2.as_str() {
                        "m-grp" => true,
                        _ => false,
                    };
                    if !useless {
                        output.push_str("<span class='");
                        output.push_str(&short_name2);
                        if short_name2 != short_name3 {
                            output.push(' ');
                            output.push_str(&short_name3);
                        }
                        output.push_str("'>");
                        open_scopes.push(Some(short_name3));
                    } else {
                        open_scopes.push(None);
                    }
                },
                ScopeStackOp::Pop(n) |
                ScopeStackOp::Clear(ClearAmount::TopN(n)) => {
                    for _ in 0..n {
                        if let Some(Some(_)) = open_scopes.pop() {
                            output.push_str("</span>");
                        }
                    }
                },
                ScopeStackOp::Clear(ClearAmount::All) |
                ScopeStackOp::Restore |
                ScopeStackOp::Noop => {},
            }
        }
        output.push_str(&line[prev_off..]);

        if iter.peek().is_some() {
            output.push('\n');
        }
    }
    output
}

fn abbreviate_scope_name(part: &str) -> &str {
    match part {
        "arguments" => "args",
        "arithmetic" => "arith",
        "assignment" => "asgn",
        "attribute-name" => "attr-n",
        "attribute-with-value" => "attr-val",
        "bash" => "sh",
        "block" => "bl",
        "boolean" => "bool",
        "cast" => "cast",
        "character" => "chr",
        "class" => "cls",
        "closure" => "closure",
        "comma" => "comma",
        "command" => "cmd",
        "comparison" => "comp",
        "compound" => "comp",
        "constant" => "const",
        "constructor" => "ctor",
        "continuation" => "cont",
        "control" => "ctrl",
        "declaration" => "decl",
        "deprecated" => "depr",
        "destructor" => "dtor",
        "dictionary" => "dict",
        "double" => "dbl",
        "double-brace" => "dbl-bl",
        "double-slash" => "dbl-sl",
        "enum" => "enum",
        "exit" => "exit",
        "expression" => "expr",
        "float" => "flt",
        "flow" => "flow",
        "function" => "fn",
        "function-call" => "fn-call",
        "generic-name" => "gen-n",
        "group" => "grp",
        "help" => "hlp",
        "html" => "html",
        "impl" => "impl",
        "instance" => "inst",
        "json" => "json",
        "key-value" => "kv",
        "keyword" => "k",
        "label" => "lbl",
        "language" => "lang",
        "lifetime" => "lf",
        "line" => "ln",
        "logical" => "logic",
        "markup" => "mk",
        "member" => "memb",
        "meta" => "m",
        "name" => "n",
        "namespace" => "ns",
        "node" => "node",
        "number-sign" => "num-sign",
        "operator" => "op",
        "other" => "ot",
        "parameter" => "parm",
        "parameters" => "parms",
        "path" => "path",
        "placeholder" => "phold",
        "plain" => "plain",
        "post-cmd" => "post-cmd",
        "preprocessor" => "prep",
        "property" => "prop",
        "python" => "py",
        "qualified-name" => "qn",
        "quoted" => "q",
        "readwrite" => "rw",
        "regexp" => "regexp",
        "return-type" => "ret-ty",
        "rust" => "rs",
        "shell" => "sh",
        "single" => "sgl",
        "source" => "src",
        "statement" => "stmt",
        "storage" => "stor",
        "struct" => "struct",
        "structure" => "struct",
        "table" => "tbl",
        "template" => "tpl",
        "template-expression" => "tpl-expr",
        "terminator" => "term",
        "text" => "txt",
        "this" => "this",
        "toml" => "toml",
        "trait" => "tr",
        "type" => "ty",
        "yaml" => "yaml",
        x => &x[0..3.min(x.len())],
    }
}

#[test]
fn parse_test() {
    let s = SyntaxSet::load_defaults_nonewlines();
    let s = s.find_syntax_by_name("Rust").expect("wat");
    assert_eq!("<span class=\'src-rs\'>wat.<span class=\'sup-fn sup-fn-rs\'>i</span><span class=\'pun-sec pun-sec-grp\'>(</span><span class=\'pun-sec pun-sec-grp\'>)</span>", &html_for_string("wat.i()", s));
}
